/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;


public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
    bc.registerService(Animal.class.getName(), new Kaczka(), null);
    Logger.get().log(this, "Kaczka przyleciala");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
    bc = null;
    Logger.get().log(this, "Kaczka odleciala");
    }
    
}
