/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal1.internal;

import pk.labs.LabC.contracts.Animal;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;


public class Kaczka implements  Animal  {
    PropertyChangeSupport pcs;
    String status;
    public Kaczka(){
        pcs = new PropertyChangeSupport(this);
    }
    @Override
    public String getSpecies() {
    return "ptaszek";
    }

    @Override
    public String getName() {
    return"fred";
    }

    @Override
    public String getStatus() {
    return status;
    }

    @Override
    public void setStatus(String status) {
    String staryStatus=this.status;
    this.status=status;
    pcs.firePropertyChange(new PropertyChangeEvent(this, "status", staryStatus, status));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    pcs.removePropertyChangeListener(listener);
    }
    
}
