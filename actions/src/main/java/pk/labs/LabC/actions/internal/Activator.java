/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;


public class Activator implements BundleActivator{
    @Override
    public void start(BundleContext bc) throws Exception {
         AnimalAction animal1 = new AnimalAction()
    {
      public boolean execute(Animal animal)
      {
        animal.setStatus("actionA1");
        return true;
      }
    };
         AnimalAction animal2 = new AnimalAction()
    {
      public boolean execute(Animal animal)
      {
        animal.setStatus("actionA2");
        return true;
      }
    };
         AnimalAction animal3 = new AnimalAction()
    {
      public boolean execute(Animal animal)
      {
        animal.setStatus("actionA3");
        return true;
      }
    };
         Hashtable prop1 = new Hashtable();
        prop1.put("species", animal1);
        bc.registerService(AnimalAction.class.getName(), animal1, prop1);

        Hashtable prop2 = new Hashtable();
        prop2.put("species", animal2);
        bc.registerService(AnimalAction.class.getName(), animal2, prop2);

        Hashtable prop3 = new Hashtable();
        prop3.put("species", animal3);
        bc.registerService(AnimalAction.class.getName(), animal3, prop3);

                 }
    @Override
    public void stop(BundleContext bc) throws Exception {
        bc = null;
    }
    
}
