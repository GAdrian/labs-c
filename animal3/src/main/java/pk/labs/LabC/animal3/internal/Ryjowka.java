/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal3.internal;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;


public class Ryjowka implements  Animal{
    PropertyChangeSupport pcs;
    String status;
    public Ryjowka(){
        pcs = new PropertyChangeSupport(this);
    }

    @Override
    public String getSpecies() {
    return "taka mysza";
    }

    @Override
    public String getName() {
    return"grucha";
    }

    @Override
    public String getStatus() {
    return status;    
    }

    @Override
    public void setStatus(String status) {
    String staryStatus=this.status;
    this.status=status;
    pcs.firePropertyChange(new PropertyChangeEvent(this, "status", staryStatus, status));  
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
    pcs.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
    pcs.removePropertyChangeListener(listener);
    }
    
}
